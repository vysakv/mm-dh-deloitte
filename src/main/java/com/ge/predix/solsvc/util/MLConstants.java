package com.ge.predix.solsvc.util;

public interface MLConstants {
	
	String DATA_POINTS_START_1YEAR="1y-ago";
	String DATA_POINTS_START_1MONTH="1mm-ago";
	String DATE_FOMAT_GETDATA="dd MMM";
	String CRITICAL="CRITICAL";
	String WARNING="WARNING";
	String NONE="NONE";
	String HEADER_AUTHORIZATION = "Authorization";
	String HEADER_PREDIX_ZONEID = "Predix-Zone-Id";
	
	int ONE_MINUTE_IN_SECONDS=60;
	int TIME_INTERVAL_MIN_EXCEL= 30;
	
	
	String ASSET_DEFAULT_NAME= "carbon";
}
