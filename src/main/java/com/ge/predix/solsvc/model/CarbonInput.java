package com.ge.predix.solsvc.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"[0,0]",
"[0,1]",
"[0,2]",
"[0,3]",
"[0,4]",
"[0,5]",
"[0,6]",
"[0,7]",
"[0,8]",
"[0,9]",
"[0,10]",
"[0,11]",
"[0,12]",
"[0,13]",
"[0,14]",
"[0,15]",
"[0,16]",
"[0,17]",
"[0,18]",
"[0,19]",
"[0,20]",
"[0,21]",
"[0,22]",
"[0,23]",
"[1,0]",
"[1,1]",
"[1,2]",
"[1,3]",
"[1,4]",
"[1,5]",
"[1,6]",
"[1,7]",
"[1,8]",
"[1,9]",
"[1,10]",
"[1,11]",
"[1,12]",
"[1,13]",
"[1,14]",
"[1,15]",
"[1,16]",
"[1,17]",
"[1,18]",
"[1,19]",
"[1,20]",
"[1,21]",
"[1,22]",
"[1,23]",
"[2,0]",
"[2,1]",
"[2,2]",
"[2,3]",
"[2,4]",
"[2,5]",
"[2,6]",
"[2,7]",
"[2,8]",
"[2,9]",
"[2,10]",
"[2,11]",
"[2,12]",
"[2,13]",
"[2,14]",
"[2,15]",
"[2,16]",
"[2,17]",
"[2,18]",
"[2,19]",
"[2,20]",
"[2,21]",
"[2,22]",
"[2,23]",
"[3,0]",
"[3,1]",
"[3,2]",
"[3,3]",
"[3,4]",
"[3,5]",
"[3,6]",
"[3,7]",
"[3,8]",
"[3,9]",
"[3,10]",
"[3,11]",
"[3,12]",
"[3,13]",
"[3,14]",
"[3,15]",
"[3,16]",
"[3,17]",
"[3,18]",
"[3,19]",
"[3,20]",
"[3,21]",
"[3,22]",
"[3,23]",
"[4,0]",
"[4,1]",
"[4,2]",
"[4,3]",
"[4,4]",
"[4,5]",
"[4,6]",
"[4,7]",
"[4,8]",
"[4,9]",
"[4,10]",
"[4,11]",
"[4,12]",
"[4,13]",
"[4,14]",
"[4,15]",
"[4,16]",
"[4,17]",
"[4,18]",
"[4,19]",
"[4,20]",
"[4,21]",
"[4,22]",
"[4,23]",
"[5,0]",
"[5,1]",
"[5,2]",
"[5,3]",
"[5,4]",
"[5,5]",
"[5,6]",
"[5,7]",
"[5,8]",
"[5,9]",
"[5,10]",
"[5,11]",
"[5,12]",
"[5,13]",
"[5,14]",
"[5,15]",
"[5,16]",
"[5,17]",
"[5,18]",
"[5,19]",
"[5,20]",
"[5,21]",
"[5,22]",
"[5,23]",
"[6,0]",
"[6,1]",
"[6,2]",
"[6,3]",
"[6,4]",
"[6,5]",
"[6,6]",
"[6,7]",
"[6,8]",
"[6,9]",
"[6,10]",
"[6,11]",
"[6,12]",
"[6,13]",
"[6,14]",
"[6,15]",
"[6,16]",
"[6,17]",
"[6,18]",
"[6,19]",
"[6,20]",
"[6,21]",
"[6,22]",
"[6,23]"
})
public class CarbonInput {

@JsonProperty("[0,0]")
private Double _00;
@JsonProperty("[0,1]")
private Double _01;
@JsonProperty("[0,2]")
private Double _02;
@JsonProperty("[0,3]")
private Double _03;
@JsonProperty("[0,4]")
private Double _04;
@JsonProperty("[0,5]")
private Double _05;
@JsonProperty("[0,6]")
private Double _06;
@JsonProperty("[0,7]")
private Double _07;
@JsonProperty("[0,8]")
private Double _08;
@JsonProperty("[0,9]")
private Double _09;
@JsonProperty("[0,10]")
private Double _010;
@JsonProperty("[0,11]")
private Double _011;
@JsonProperty("[0,12]")
private Double _012;
@JsonProperty("[0,13]")
private Double _013;
@JsonProperty("[0,14]")
private Double _014;
@JsonProperty("[0,15]")
private Double _015;
@JsonProperty("[0,16]")
private Double _016;
@JsonProperty("[0,17]")
private Double _017;
@JsonProperty("[0,18]")
private Double _018;
@JsonProperty("[0,19]")
private Double _019;
@JsonProperty("[0,20]")
private Double _020;
@JsonProperty("[0,21]")
private Double _021;
@JsonProperty("[0,22]")
private Double _022;
@JsonProperty("[0,23]")
private Double _023;
@JsonProperty("[1,0]")
private Double _10;
@JsonProperty("[1,1]")
private Double _11;
@JsonProperty("[1,2]")
private Double _12;
@JsonProperty("[1,3]")
private Double _13;
@JsonProperty("[1,4]")
private Double _14;
@JsonProperty("[1,5]")
private Double _15;
@JsonProperty("[1,6]")
private Double _16;
@JsonProperty("[1,7]")
private Double _17;
@JsonProperty("[1,8]")
private Double _18;
@JsonProperty("[1,9]")
private Double _19;
@JsonProperty("[1,10]")
private Double _110;
@JsonProperty("[1,11]")
private Double _111;
@JsonProperty("[1,12]")
private Double _112;
@JsonProperty("[1,13]")
private Double _113;
@JsonProperty("[1,14]")
private Double _114;
@JsonProperty("[1,15]")
private Double _115;
@JsonProperty("[1,16]")
private Double _116;
@JsonProperty("[1,17]")
private Double _117;
@JsonProperty("[1,18]")
private Double _118;
@JsonProperty("[1,19]")
private Double _119;
@JsonProperty("[1,20]")
private Double _120;
@JsonProperty("[1,21]")
private Double _121;
@JsonProperty("[1,22]")
private Double _122;
@JsonProperty("[1,23]")
private Double _123;
@JsonProperty("[2,0]")
private Double _20;
@JsonProperty("[2,1]")
private Double _21;
@JsonProperty("[2,2]")
private Double _22;
@JsonProperty("[2,3]")
private Double _23;
@JsonProperty("[2,4]")
private Double _24;
@JsonProperty("[2,5]")
private Double _25;
@JsonProperty("[2,6]")
private Double _26;
@JsonProperty("[2,7]")
private Double _27;
@JsonProperty("[2,8]")
private Double _28;
@JsonProperty("[2,9]")
private Double _29;
@JsonProperty("[2,10]")
private Double _210;
@JsonProperty("[2,11]")
private Double _211;
@JsonProperty("[2,12]")
private Double _212;
@JsonProperty("[2,13]")
private Double _213;
@JsonProperty("[2,14]")
private Double _214;
@JsonProperty("[2,15]")
private Double _215;
@JsonProperty("[2,16]")
private Double _216;
@JsonProperty("[2,17]")
private Double _217;
@JsonProperty("[2,18]")
private Double _218;
@JsonProperty("[2,19]")
private Double _219;
@JsonProperty("[2,20]")
private Double _220;
@JsonProperty("[2,21]")
private Double _221;
@JsonProperty("[2,22]")
private Double _222;
@JsonProperty("[2,23]")
private Double _223;
@JsonProperty("[3,0]")
private Double _30;
@JsonProperty("[3,1]")
private Double _31;
@JsonProperty("[3,2]")
private Double _32;
@JsonProperty("[3,3]")
private Double _33;
@JsonProperty("[3,4]")
private Double _34;
@JsonProperty("[3,5]")
private Double _35;
@JsonProperty("[3,6]")
private Double _36;
@JsonProperty("[3,7]")
private Double _37;
@JsonProperty("[3,8]")
private Double _38;
@JsonProperty("[3,9]")
private Double _39;
@JsonProperty("[3,10]")
private Double _310;
@JsonProperty("[3,11]")
private Double _311;
@JsonProperty("[3,12]")
private Double _312;
@JsonProperty("[3,13]")
private Double _313;
@JsonProperty("[3,14]")
private Double _314;
@JsonProperty("[3,15]")
private Double _315;
@JsonProperty("[3,16]")
private Double _316;
@JsonProperty("[3,17]")
private Double _317;
@JsonProperty("[3,18]")
private Double _318;
@JsonProperty("[3,19]")
private Double _319;
@JsonProperty("[3,20]")
private Double _320;
@JsonProperty("[3,21]")
private Double _321;
@JsonProperty("[3,22]")
private Double _322;
@JsonProperty("[3,23]")
private Double _323;
@JsonProperty("[4,0]")
private Double _40;
@JsonProperty("[4,1]")
private Double _41;
@JsonProperty("[4,2]")
private Double _42;
@JsonProperty("[4,3]")
private Double _43;
@JsonProperty("[4,4]")
private Double _44;
@JsonProperty("[4,5]")
private Double _45;
@JsonProperty("[4,6]")
private Double _46;
@JsonProperty("[4,7]")
private Double _47;
@JsonProperty("[4,8]")
private Double _48;
@JsonProperty("[4,9]")
private Double _49;
@JsonProperty("[4,10]")
private Double _410;
@JsonProperty("[4,11]")
private Double _411;
@JsonProperty("[4,12]")
private Double _412;
@JsonProperty("[4,13]")
private Double _413;
@JsonProperty("[4,14]")
private Double _414;
@JsonProperty("[4,15]")
private Double _415;
@JsonProperty("[4,16]")
private Double _416;
@JsonProperty("[4,17]")
private Double _417;
@JsonProperty("[4,18]")
private Double _418;
@JsonProperty("[4,19]")
private Double _419;
@JsonProperty("[4,20]")
private Double _420;
@JsonProperty("[4,21]")
private Double _421;
@JsonProperty("[4,22]")
private Double _422;
@JsonProperty("[4,23]")
private Double _423;
@JsonProperty("[5,0]")
private Double _50;
@JsonProperty("[5,1]")
private Double _51;
@JsonProperty("[5,2]")
private Double _52;
@JsonProperty("[5,3]")
private Double _53;
@JsonProperty("[5,4]")
private Double _54;
@JsonProperty("[5,5]")
private Double _55;
@JsonProperty("[5,6]")
private Double _56;
@JsonProperty("[5,7]")
private Double _57;
@JsonProperty("[5,8]")
private Double _58;
@JsonProperty("[5,9]")
private Double _59;
@JsonProperty("[5,10]")
private Double _510;
@JsonProperty("[5,11]")
private Double _511;
@JsonProperty("[5,12]")
private Double _512;
@JsonProperty("[5,13]")
private Double _513;
@JsonProperty("[5,14]")
private Double _514;
@JsonProperty("[5,15]")
private Double _515;
@JsonProperty("[5,16]")
private Double _516;
@JsonProperty("[5,17]")
private Double _517;
@JsonProperty("[5,18]")
private Double _518;
@JsonProperty("[5,19]")
private Double _519;
@JsonProperty("[5,20]")
private Double _520;
@JsonProperty("[5,21]")
private Double _521;
@JsonProperty("[5,22]")
private Double _522;
@JsonProperty("[5,23]")
private Double _523;
@JsonProperty("[6,0]")
private Double _60;
@JsonProperty("[6,1]")
private Double _61;
@JsonProperty("[6,2]")
private Double _62;
@JsonProperty("[6,3]")
private Double _63;
@JsonProperty("[6,4]")
private Double _64;
@JsonProperty("[6,5]")
private Double _65;
@JsonProperty("[6,6]")
private Double _66;
@JsonProperty("[6,7]")
private Double _67;
@JsonProperty("[6,8]")
private Double _68;
@JsonProperty("[6,9]")
private Double _69;
@JsonProperty("[6,10]")
private Double _610;
@JsonProperty("[6,11]")
private Double _611;
@JsonProperty("[6,12]")
private Double _612;
@JsonProperty("[6,13]")
private Double _613;
@JsonProperty("[6,14]")
private Double _614;
@JsonProperty("[6,15]")
private Double _615;
@JsonProperty("[6,16]")
private Double _616;
@JsonProperty("[6,17]")
private Double _617;
@JsonProperty("[6,18]")
private Double _618;
@JsonProperty("[6,19]")
private Double _619;
@JsonProperty("[6,20]")
private Double _620;
@JsonProperty("[6,21]")
private Double _621;
@JsonProperty("[6,22]")
private Double _622;
@JsonProperty("[6,23]")
private Double _623;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("[0,0]")
public Double get00() {
return _00;
}

@JsonProperty("[0,0]")
public void set00(Double _00) {
this._00 = _00;
}

@JsonProperty("[0,1]")
public Double get01() {
return _01;
}

@JsonProperty("[0,1]")
public void set01(Double _01) {
this._01 = _01;
}

@JsonProperty("[0,2]")
public Double get02() {
return _02;
}

@JsonProperty("[0,2]")
public void set02(Double _02) {
this._02 = _02;
}

@JsonProperty("[0,3]")
public Double get03() {
return _03;
}

@JsonProperty("[0,3]")
public void set03(Double _03) {
this._03 = _03;
}

@JsonProperty("[0,4]")
public Double get04() {
return _04;
}

@JsonProperty("[0,4]")
public void set04(Double _04) {
this._04 = _04;
}

@JsonProperty("[0,5]")
public Double get05() {
return _05;
}

@JsonProperty("[0,5]")
public void set05(Double _05) {
this._05 = _05;
}

@JsonProperty("[0,6]")
public Double get06() {
return _06;
}

@JsonProperty("[0,6]")
public void set06(Double _06) {
this._06 = _06;
}

@JsonProperty("[0,7]")
public Double get07() {
return _07;
}

@JsonProperty("[0,7]")
public void set07(Double _07) {
this._07 = _07;
}

@JsonProperty("[0,8]")
public Double get08() {
return _08;
}

@JsonProperty("[0,8]")
public void set08(Double _08) {
this._08 = _08;
}

@JsonProperty("[0,9]")
public Double get09() {
return _09;
}

@JsonProperty("[0,9]")
public void set09(Double _09) {
this._09 = _09;
}

@JsonProperty("[0,10]")
public Double get010() {
return _010;
}

@JsonProperty("[0,10]")
public void set010(Double _010) {
this._010 = _010;
}

@JsonProperty("[0,11]")
public Double get011() {
return _011;
}

@JsonProperty("[0,11]")
public void set011(Double _011) {
this._011 = _011;
}

@JsonProperty("[0,12]")
public Double get012() {
return _012;
}

@JsonProperty("[0,12]")
public void set012(Double _012) {
this._012 = _012;
}

@JsonProperty("[0,13]")
public Double get013() {
return _013;
}

@JsonProperty("[0,13]")
public void set013(Double _013) {
this._013 = _013;
}

@JsonProperty("[0,14]")
public Double get014() {
return _014;
}

@JsonProperty("[0,14]")
public void set014(Double _014) {
this._014 = _014;
}

@JsonProperty("[0,15]")
public Double get015() {
return _015;
}

@JsonProperty("[0,15]")
public void set015(Double _015) {
this._015 = _015;
}

@JsonProperty("[0,16]")
public Double get016() {
return _016;
}

@JsonProperty("[0,16]")
public void set016(Double _016) {
this._016 = _016;
}

@JsonProperty("[0,17]")
public Double get017() {
return _017;
}

@JsonProperty("[0,17]")
public void set017(Double _017) {
this._017 = _017;
}

@JsonProperty("[0,18]")
public Double get018() {
return _018;
}

@JsonProperty("[0,18]")
public void set018(Double _018) {
this._018 = _018;
}

@JsonProperty("[0,19]")
public Double get019() {
return _019;
}

@JsonProperty("[0,19]")
public void set019(Double _019) {
this._019 = _019;
}

@JsonProperty("[0,20]")
public Double get020() {
return _020;
}

@JsonProperty("[0,20]")
public void set020(Double _020) {
this._020 = _020;
}

@JsonProperty("[0,21]")
public Double get021() {
return _021;
}

@JsonProperty("[0,21]")
public void set021(Double _021) {
this._021 = _021;
}

@JsonProperty("[0,22]")
public Double get022() {
return _022;
}

@JsonProperty("[0,22]")
public void set022(Double _022) {
this._022 = _022;
}

@JsonProperty("[0,23]")
public Double get023() {
return _023;
}

@JsonProperty("[0,23]")
public void set023(Double _023) {
this._023 = _023;
}

@JsonProperty("[1,0]")
public Double get10() {
return _10;
}

@JsonProperty("[1,0]")
public void set10(Double _10) {
this._10 = _10;
}

@JsonProperty("[1,1]")
public Double get11() {
return _11;
}

@JsonProperty("[1,1]")
public void set11(Double _11) {
this._11 = _11;
}

@JsonProperty("[1,2]")
public Double get12() {
return _12;
}

@JsonProperty("[1,2]")
public void set12(Double _12) {
this._12 = _12;
}

@JsonProperty("[1,3]")
public Double get13() {
return _13;
}

@JsonProperty("[1,3]")
public void set13(Double _13) {
this._13 = _13;
}

@JsonProperty("[1,4]")
public Double get14() {
return _14;
}

@JsonProperty("[1,4]")
public void set14(Double _14) {
this._14 = _14;
}

@JsonProperty("[1,5]")
public Double get15() {
return _15;
}

@JsonProperty("[1,5]")
public void set15(Double _15) {
this._15 = _15;
}

@JsonProperty("[1,6]")
public Double get16() {
return _16;
}

@JsonProperty("[1,6]")
public void set16(Double _16) {
this._16 = _16;
}

@JsonProperty("[1,7]")
public Double get17() {
return _17;
}

@JsonProperty("[1,7]")
public void set17(Double _17) {
this._17 = _17;
}

@JsonProperty("[1,8]")
public Double get18() {
return _18;
}

@JsonProperty("[1,8]")
public void set18(Double _18) {
this._18 = _18;
}

@JsonProperty("[1,9]")
public Double get19() {
return _19;
}

@JsonProperty("[1,9]")
public void set19(Double _19) {
this._19 = _19;
}

@JsonProperty("[1,10]")
public Double get110() {
return _110;
}

@JsonProperty("[1,10]")
public void set110(Double _110) {
this._110 = _110;
}

@JsonProperty("[1,11]")
public Double get111() {
return _111;
}

@JsonProperty("[1,11]")
public void set111(Double _111) {
this._111 = _111;
}

@JsonProperty("[1,12]")
public Double get112() {
return _112;
}

@JsonProperty("[1,12]")
public void set112(Double _112) {
this._112 = _112;
}

@JsonProperty("[1,13]")
public Double get113() {
return _113;
}

@JsonProperty("[1,13]")
public void set113(Double _113) {
this._113 = _113;
}

@JsonProperty("[1,14]")
public Double get114() {
return _114;
}

@JsonProperty("[1,14]")
public void set114(Double _114) {
this._114 = _114;
}

@JsonProperty("[1,15]")
public Double get115() {
return _115;
}

@JsonProperty("[1,15]")
public void set115(Double _115) {
this._115 = _115;
}

@JsonProperty("[1,16]")
public Double get116() {
return _116;
}

@JsonProperty("[1,16]")
public void set116(Double _116) {
this._116 = _116;
}

@JsonProperty("[1,17]")
public Double get117() {
return _117;
}

@JsonProperty("[1,17]")
public void set117(Double _117) {
this._117 = _117;
}

@JsonProperty("[1,18]")
public Double get118() {
return _118;
}

@JsonProperty("[1,18]")
public void set118(Double _118) {
this._118 = _118;
}

@JsonProperty("[1,19]")
public Double get119() {
return _119;
}

@JsonProperty("[1,19]")
public void set119(Double _119) {
this._119 = _119;
}

@JsonProperty("[1,20]")
public Double get120() {
return _120;
}

@JsonProperty("[1,20]")
public void set120(Double _120) {
this._120 = _120;
}

@JsonProperty("[1,21]")
public Double get121() {
return _121;
}

@JsonProperty("[1,21]")
public void set121(Double _121) {
this._121 = _121;
}

@JsonProperty("[1,22]")
public Double get122() {
return _122;
}

@JsonProperty("[1,22]")
public void set122(Double _122) {
this._122 = _122;
}

@JsonProperty("[1,23]")
public Double get123() {
return _123;
}

@JsonProperty("[1,23]")
public void set123(Double _123) {
this._123 = _123;
}

@JsonProperty("[2,0]")
public Double get20() {
return _20;
}

@JsonProperty("[2,0]")
public void set20(Double _20) {
this._20 = _20;
}

@JsonProperty("[2,1]")
public Double get21() {
return _21;
}

@JsonProperty("[2,1]")
public void set21(Double _21) {
this._21 = _21;
}

@JsonProperty("[2,2]")
public Double get22() {
return _22;
}

@JsonProperty("[2,2]")
public void set22(Double _22) {
this._22 = _22;
}

@JsonProperty("[2,3]")
public Double get23() {
return _23;
}

@JsonProperty("[2,3]")
public void set23(Double _23) {
this._23 = _23;
}

@JsonProperty("[2,4]")
public Double get24() {
return _24;
}

@JsonProperty("[2,4]")
public void set24(Double _24) {
this._24 = _24;
}

@JsonProperty("[2,5]")
public Double get25() {
return _25;
}

@JsonProperty("[2,5]")
public void set25(Double _25) {
this._25 = _25;
}

@JsonProperty("[2,6]")
public Double get26() {
return _26;
}

@JsonProperty("[2,6]")
public void set26(Double _26) {
this._26 = _26;
}

@JsonProperty("[2,7]")
public Double get27() {
return _27;
}

@JsonProperty("[2,7]")
public void set27(Double _27) {
this._27 = _27;
}

@JsonProperty("[2,8]")
public Double get28() {
return _28;
}

@JsonProperty("[2,8]")
public void set28(Double _28) {
this._28 = _28;
}

@JsonProperty("[2,9]")
public Double get29() {
return _29;
}

@JsonProperty("[2,9]")
public void set29(Double _29) {
this._29 = _29;
}

@JsonProperty("[2,10]")
public Double get210() {
return _210;
}

@JsonProperty("[2,10]")
public void set210(Double _210) {
this._210 = _210;
}

@JsonProperty("[2,11]")
public Double get211() {
return _211;
}

@JsonProperty("[2,11]")
public void set211(Double _211) {
this._211 = _211;
}

@JsonProperty("[2,12]")
public Double get212() {
return _212;
}

@JsonProperty("[2,12]")
public void set212(Double _212) {
this._212 = _212;
}

@JsonProperty("[2,13]")
public Double get213() {
return _213;
}

@JsonProperty("[2,13]")
public void set213(Double _213) {
this._213 = _213;
}

@JsonProperty("[2,14]")
public Double get214() {
return _214;
}

@JsonProperty("[2,14]")
public void set214(Double _214) {
this._214 = _214;
}

@JsonProperty("[2,15]")
public Double get215() {
return _215;
}

@JsonProperty("[2,15]")
public void set215(Double _215) {
this._215 = _215;
}

@JsonProperty("[2,16]")
public Double get216() {
return _216;
}

@JsonProperty("[2,16]")
public void set216(Double _216) {
this._216 = _216;
}

@JsonProperty("[2,17]")
public Double get217() {
return _217;
}

@JsonProperty("[2,17]")
public void set217(Double _217) {
this._217 = _217;
}

@JsonProperty("[2,18]")
public Double get218() {
return _218;
}

@JsonProperty("[2,18]")
public void set218(Double _218) {
this._218 = _218;
}

@JsonProperty("[2,19]")
public Double get219() {
return _219;
}

@JsonProperty("[2,19]")
public void set219(Double _219) {
this._219 = _219;
}

@JsonProperty("[2,20]")
public Double get220() {
return _220;
}

@JsonProperty("[2,20]")
public void set220(Double _220) {
this._220 = _220;
}

@JsonProperty("[2,21]")
public Double get221() {
return _221;
}

@JsonProperty("[2,21]")
public void set221(Double _221) {
this._221 = _221;
}

@JsonProperty("[2,22]")
public Double get222() {
return _222;
}

@JsonProperty("[2,22]")
public void set222(Double _222) {
this._222 = _222;
}

@JsonProperty("[2,23]")
public Double get223() {
return _223;
}

@JsonProperty("[2,23]")
public void set223(Double _223) {
this._223 = _223;
}

@JsonProperty("[3,0]")
public Double get30() {
return _30;
}

@JsonProperty("[3,0]")
public void set30(Double _30) {
this._30 = _30;
}

@JsonProperty("[3,1]")
public Double get31() {
return _31;
}

@JsonProperty("[3,1]")
public void set31(Double _31) {
this._31 = _31;
}

@JsonProperty("[3,2]")
public Double get32() {
return _32;
}

@JsonProperty("[3,2]")
public void set32(Double _32) {
this._32 = _32;
}

@JsonProperty("[3,3]")
public Double get33() {
return _33;
}

@JsonProperty("[3,3]")
public void set33(Double _33) {
this._33 = _33;
}

@JsonProperty("[3,4]")
public Double get34() {
return _34;
}

@JsonProperty("[3,4]")
public void set34(Double _34) {
this._34 = _34;
}

@JsonProperty("[3,5]")
public Double get35() {
return _35;
}

@JsonProperty("[3,5]")
public void set35(Double _35) {
this._35 = _35;
}

@JsonProperty("[3,6]")
public Double get36() {
return _36;
}

@JsonProperty("[3,6]")
public void set36(Double _36) {
this._36 = _36;
}

@JsonProperty("[3,7]")
public Double get37() {
return _37;
}

@JsonProperty("[3,7]")
public void set37(Double _37) {
this._37 = _37;
}

@JsonProperty("[3,8]")
public Double get38() {
return _38;
}

@JsonProperty("[3,8]")
public void set38(Double _38) {
this._38 = _38;
}

@JsonProperty("[3,9]")
public Double get39() {
return _39;
}

@JsonProperty("[3,9]")
public void set39(Double _39) {
this._39 = _39;
}

@JsonProperty("[3,10]")
public Double get310() {
return _310;
}

@JsonProperty("[3,10]")
public void set310(Double _310) {
this._310 = _310;
}

@JsonProperty("[3,11]")
public Double get311() {
return _311;
}

@JsonProperty("[3,11]")
public void set311(Double _311) {
this._311 = _311;
}

@JsonProperty("[3,12]")
public Double get312() {
return _312;
}

@JsonProperty("[3,12]")
public void set312(Double _312) {
this._312 = _312;
}

@JsonProperty("[3,13]")
public Double get313() {
return _313;
}

@JsonProperty("[3,13]")
public void set313(Double _313) {
this._313 = _313;
}

@JsonProperty("[3,14]")
public Double get314() {
return _314;
}

@JsonProperty("[3,14]")
public void set314(Double _314) {
this._314 = _314;
}

@JsonProperty("[3,15]")
public Double get315() {
return _315;
}

@JsonProperty("[3,15]")
public void set315(Double _315) {
this._315 = _315;
}

@JsonProperty("[3,16]")
public Double get316() {
return _316;
}

@JsonProperty("[3,16]")
public void set316(Double _316) {
this._316 = _316;
}

@JsonProperty("[3,17]")
public Double get317() {
return _317;
}

@JsonProperty("[3,17]")
public void set317(Double _317) {
this._317 = _317;
}

@JsonProperty("[3,18]")
public Double get318() {
return _318;
}

@JsonProperty("[3,18]")
public void set318(Double _318) {
this._318 = _318;
}

@JsonProperty("[3,19]")
public Double get319() {
return _319;
}

@JsonProperty("[3,19]")
public void set319(Double _319) {
this._319 = _319;
}

@JsonProperty("[3,20]")
public Double get320() {
return _320;
}

@JsonProperty("[3,20]")
public void set320(Double _320) {
this._320 = _320;
}

@JsonProperty("[3,21]")
public Double get321() {
return _321;
}

@JsonProperty("[3,21]")
public void set321(Double _321) {
this._321 = _321;
}

@JsonProperty("[3,22]")
public Double get322() {
return _322;
}

@JsonProperty("[3,22]")
public void set322(Double _322) {
this._322 = _322;
}

@JsonProperty("[3,23]")
public Double get323() {
return _323;
}

@JsonProperty("[3,23]")
public void set323(Double _323) {
this._323 = _323;
}

@JsonProperty("[4,0]")
public Double get40() {
return _40;
}

@JsonProperty("[4,0]")
public void set40(Double _40) {
this._40 = _40;
}

@JsonProperty("[4,1]")
public Double get41() {
return _41;
}

@JsonProperty("[4,1]")
public void set41(Double _41) {
this._41 = _41;
}

@JsonProperty("[4,2]")
public Double get42() {
return _42;
}

@JsonProperty("[4,2]")
public void set42(Double _42) {
this._42 = _42;
}

@JsonProperty("[4,3]")
public Double get43() {
return _43;
}

@JsonProperty("[4,3]")
public void set43(Double _43) {
this._43 = _43;
}

@JsonProperty("[4,4]")
public Double get44() {
return _44;
}

@JsonProperty("[4,4]")
public void set44(Double _44) {
this._44 = _44;
}

@JsonProperty("[4,5]")
public Double get45() {
return _45;
}

@JsonProperty("[4,5]")
public void set45(Double _45) {
this._45 = _45;
}

@JsonProperty("[4,6]")
public Double get46() {
return _46;
}

@JsonProperty("[4,6]")
public void set46(Double _46) {
this._46 = _46;
}

@JsonProperty("[4,7]")
public Double get47() {
return _47;
}

@JsonProperty("[4,7]")
public void set47(Double _47) {
this._47 = _47;
}

@JsonProperty("[4,8]")
public Double get48() {
return _48;
}

@JsonProperty("[4,8]")
public void set48(Double _48) {
this._48 = _48;
}

@JsonProperty("[4,9]")
public Double get49() {
return _49;
}

@JsonProperty("[4,9]")
public void set49(Double _49) {
this._49 = _49;
}

@JsonProperty("[4,10]")
public Double get410() {
return _410;
}

@JsonProperty("[4,10]")
public void set410(Double _410) {
this._410 = _410;
}

@JsonProperty("[4,11]")
public Double get411() {
return _411;
}

@JsonProperty("[4,11]")
public void set411(Double _411) {
this._411 = _411;
}

@JsonProperty("[4,12]")
public Double get412() {
return _412;
}

@JsonProperty("[4,12]")
public void set412(Double _412) {
this._412 = _412;
}

@JsonProperty("[4,13]")
public Double get413() {
return _413;
}

@JsonProperty("[4,13]")
public void set413(Double _413) {
this._413 = _413;
}

@JsonProperty("[4,14]")
public Double get414() {
return _414;
}

@JsonProperty("[4,14]")
public void set414(Double _414) {
this._414 = _414;
}

@JsonProperty("[4,15]")
public Double get415() {
return _415;
}

@JsonProperty("[4,15]")
public void set415(Double _415) {
this._415 = _415;
}

@JsonProperty("[4,16]")
public Double get416() {
return _416;
}

@JsonProperty("[4,16]")
public void set416(Double _416) {
this._416 = _416;
}

@JsonProperty("[4,17]")
public Double get417() {
return _417;
}

@JsonProperty("[4,17]")
public void set417(Double _417) {
this._417 = _417;
}

@JsonProperty("[4,18]")
public Double get418() {
return _418;
}

@JsonProperty("[4,18]")
public void set418(Double _418) {
this._418 = _418;
}

@JsonProperty("[4,19]")
public Double get419() {
return _419;
}

@JsonProperty("[4,19]")
public void set419(Double _419) {
this._419 = _419;
}

@JsonProperty("[4,20]")
public Double get420() {
return _420;
}

@JsonProperty("[4,20]")
public void set420(Double _420) {
this._420 = _420;
}

@JsonProperty("[4,21]")
public Double get421() {
return _421;
}

@JsonProperty("[4,21]")
public void set421(Double _421) {
this._421 = _421;
}

@JsonProperty("[4,22]")
public Double get422() {
return _422;
}

@JsonProperty("[4,22]")
public void set422(Double _422) {
this._422 = _422;
}

@JsonProperty("[4,23]")
public Double get423() {
return _423;
}

@JsonProperty("[4,23]")
public void set423(Double _423) {
this._423 = _423;
}

@JsonProperty("[5,0]")
public Double get50() {
return _50;
}

@JsonProperty("[5,0]")
public void set50(Double _50) {
this._50 = _50;
}

@JsonProperty("[5,1]")
public Double get51() {
return _51;
}

@JsonProperty("[5,1]")
public void set51(Double _51) {
this._51 = _51;
}

@JsonProperty("[5,2]")
public Double get52() {
return _52;
}

@JsonProperty("[5,2]")
public void set52(Double _52) {
this._52 = _52;
}

@JsonProperty("[5,3]")
public Double get53() {
return _53;
}

@JsonProperty("[5,3]")
public void set53(Double _53) {
this._53 = _53;
}

@JsonProperty("[5,4]")
public Double get54() {
return _54;
}

@JsonProperty("[5,4]")
public void set54(Double _54) {
this._54 = _54;
}

@JsonProperty("[5,5]")
public Double get55() {
return _55;
}

@JsonProperty("[5,5]")
public void set55(Double _55) {
this._55 = _55;
}

@JsonProperty("[5,6]")
public Double get56() {
return _56;
}

@JsonProperty("[5,6]")
public void set56(Double _56) {
this._56 = _56;
}

@JsonProperty("[5,7]")
public Double get57() {
return _57;
}

@JsonProperty("[5,7]")
public void set57(Double _57) {
this._57 = _57;
}

@JsonProperty("[5,8]")
public Double get58() {
return _58;
}

@JsonProperty("[5,8]")
public void set58(Double _58) {
this._58 = _58;
}

@JsonProperty("[5,9]")
public Double get59() {
return _59;
}

@JsonProperty("[5,9]")
public void set59(Double _59) {
this._59 = _59;
}

@JsonProperty("[5,10]")
public Double get510() {
return _510;
}

@JsonProperty("[5,10]")
public void set510(Double _510) {
this._510 = _510;
}

@JsonProperty("[5,11]")
public Double get511() {
return _511;
}

@JsonProperty("[5,11]")
public void set511(Double _511) {
this._511 = _511;
}

@JsonProperty("[5,12]")
public Double get512() {
return _512;
}

@JsonProperty("[5,12]")
public void set512(Double _512) {
this._512 = _512;
}

@JsonProperty("[5,13]")
public Double get513() {
return _513;
}

@JsonProperty("[5,13]")
public void set513(Double _513) {
this._513 = _513;
}

@JsonProperty("[5,14]")
public Double get514() {
return _514;
}

@JsonProperty("[5,14]")
public void set514(Double _514) {
this._514 = _514;
}

@JsonProperty("[5,15]")
public Double get515() {
return _515;
}

@JsonProperty("[5,15]")
public void set515(Double _515) {
this._515 = _515;
}

@JsonProperty("[5,16]")
public Double get516() {
return _516;
}

@JsonProperty("[5,16]")
public void set516(Double _516) {
this._516 = _516;
}

@JsonProperty("[5,17]")
public Double get517() {
return _517;
}

@JsonProperty("[5,17]")
public void set517(Double _517) {
this._517 = _517;
}

@JsonProperty("[5,18]")
public Double get518() {
return _518;
}

@JsonProperty("[5,18]")
public void set518(Double _518) {
this._518 = _518;
}

@JsonProperty("[5,19]")
public Double get519() {
return _519;
}

@JsonProperty("[5,19]")
public void set519(Double _519) {
this._519 = _519;
}

@JsonProperty("[5,20]")
public Double get520() {
return _520;
}

@JsonProperty("[5,20]")
public void set520(Double _520) {
this._520 = _520;
}

@JsonProperty("[5,21]")
public Double get521() {
return _521;
}

@JsonProperty("[5,21]")
public void set521(Double _521) {
this._521 = _521;
}

@JsonProperty("[5,22]")
public Double get522() {
return _522;
}

@JsonProperty("[5,22]")
public void set522(Double _522) {
this._522 = _522;
}

@JsonProperty("[5,23]")
public Double get523() {
return _523;
}

@JsonProperty("[5,23]")
public void set523(Double _523) {
this._523 = _523;
}

@JsonProperty("[6,0]")
public Double get60() {
return _60;
}

@JsonProperty("[6,0]")
public void set60(Double _60) {
this._60 = _60;
}

@JsonProperty("[6,1]")
public Double get61() {
return _61;
}

@JsonProperty("[6,1]")
public void set61(Double _61) {
this._61 = _61;
}

@JsonProperty("[6,2]")
public Double get62() {
return _62;
}

@JsonProperty("[6,2]")
public void set62(Double _62) {
this._62 = _62;
}

@JsonProperty("[6,3]")
public Double get63() {
return _63;
}

@JsonProperty("[6,3]")
public void set63(Double _63) {
this._63 = _63;
}

@JsonProperty("[6,4]")
public Double get64() {
return _64;
}

@JsonProperty("[6,4]")
public void set64(Double _64) {
this._64 = _64;
}

@JsonProperty("[6,5]")
public Double get65() {
return _65;
}

@JsonProperty("[6,5]")
public void set65(Double _65) {
this._65 = _65;
}

@JsonProperty("[6,6]")
public Double get66() {
return _66;
}

@JsonProperty("[6,6]")
public void set66(Double _66) {
this._66 = _66;
}

@JsonProperty("[6,7]")
public Double get67() {
return _67;
}

@JsonProperty("[6,7]")
public void set67(Double _67) {
this._67 = _67;
}

@JsonProperty("[6,8]")
public Double get68() {
return _68;
}

@JsonProperty("[6,8]")
public void set68(Double _68) {
this._68 = _68;
}

@JsonProperty("[6,9]")
public Double get69() {
return _69;
}

@JsonProperty("[6,9]")
public void set69(Double _69) {
this._69 = _69;
}

@JsonProperty("[6,10]")
public Double get610() {
return _610;
}

@JsonProperty("[6,10]")
public void set610(Double _610) {
this._610 = _610;
}

@JsonProperty("[6,11]")
public Double get611() {
return _611;
}

@JsonProperty("[6,11]")
public void set611(Double _611) {
this._611 = _611;
}

@JsonProperty("[6,12]")
public Double get612() {
return _612;
}

@JsonProperty("[6,12]")
public void set612(Double _612) {
this._612 = _612;
}

@JsonProperty("[6,13]")
public Double get613() {
return _613;
}

@JsonProperty("[6,13]")
public void set613(Double _613) {
this._613 = _613;
}

@JsonProperty("[6,14]")
public Double get614() {
return _614;
}

@JsonProperty("[6,14]")
public void set614(Double _614) {
this._614 = _614;
}

@JsonProperty("[6,15]")
public Double get615() {
return _615;
}

@JsonProperty("[6,15]")
public void set615(Double _615) {
this._615 = _615;
}

@JsonProperty("[6,16]")
public Double get616() {
return _616;
}

@JsonProperty("[6,16]")
public void set616(Double _616) {
this._616 = _616;
}

@JsonProperty("[6,17]")
public Double get617() {
return _617;
}

@JsonProperty("[6,17]")
public void set617(Double _617) {
this._617 = _617;
}

@JsonProperty("[6,18]")
public Double get618() {
return _618;
}

@JsonProperty("[6,18]")
public void set618(Double _618) {
this._618 = _618;
}

@JsonProperty("[6,19]")
public Double get619() {
return _619;
}

@JsonProperty("[6,19]")
public void set619(Double _619) {
this._619 = _619;
}

@JsonProperty("[6,20]")
public Double get620() {
return _620;
}

@JsonProperty("[6,20]")
public void set620(Double _620) {
this._620 = _620;
}

@JsonProperty("[6,21]")
public Double get621() {
return _621;
}

@JsonProperty("[6,21]")
public void set621(Double _621) {
this._621 = _621;
}

@JsonProperty("[6,22]")
public Double get622() {
return _622;
}

@JsonProperty("[6,22]")
public void set622(Double _622) {
this._622 = _622;
}

@JsonProperty("[6,23]")
public Double get623() {
return _623;
}

@JsonProperty("[6,23]")
public void set623(Double _623) {
this._623 = _623;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}