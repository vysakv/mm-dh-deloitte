/**
 * 
 */
package com.ge.predix.solsvc.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.http.Header;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ge.predix.entity.timeseries.datapoints.queryrequest.DatapointsQuery;
import com.ge.predix.entity.timeseries.datapoints.queryrequest.Tag;
import com.ge.predix.entity.timeseries.datapoints.queryresponse.DatapointsResponse;
import com.ge.predix.entity.timeseries.tags.TagsList;
import com.ge.predix.solsvc.model.CarbonAsset;
import com.ge.predix.solsvc.util.MLConstants;

import scala.util.parsing.json.JSON;

import com.ge.predix.solsvc.timeseries.bootstrap.client.TimeseriesClient;

/**
 * @author vvenkateswaran
 *
 */
@Service
public class MLService {
	

	private static final Logger LOG = LoggerFactory.getLogger(MLService.class);
	@Autowired
	private TimeseriesClient client;

	@Value(value = "${predix.asset.service.url}")
	private String predixAssetServiceUrl;

	@Value(value = "${predix.asset.service.zoneid}")
	private String assetZoneId;
	
	//@Value(value = "${predix.asset.service.token}")
	private String token;

	
	
	/**
	 * Package ML training and prediction datasets
	 * 
	 * @param assetName
	 * @return MLInputData
	 */
	public DatapointsResponse queryForDatapoints(String startDuration, int taglimit,List<String> searchTagList){
		
		DatapointsResponse response = null;
		
		
		try {
			
			DatapointsQuery dpq = new DatapointsQuery();
			dpq.setStart(startDuration);
			dpq.setTags(getTagListFromTimeSeries(searchTagList,taglimit));
			response = client.queryForDatapoints(dpq, client.getTimeseriesHeaders());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}
	
	/**
	 * Package ML training and prediction datasets
	 * 
	 * @param assetName
	 * @return MLInputData
	 */
	private List<Tag> getTagListFromTimeSeries(List<String> searchTagList,int taglimit){
		
		
		TagsList s = client.listTags(client.getTimeseriesHeaders());
		List<Tag> listOfTags = new ArrayList<Tag>();
		int count = 0;
		
		for (String tag : s.getResults()) {
			LOG.info("tag --" + tag);
			Tag a = new Tag();
			a.setName(tag);
			/**if (containsAKeyword(tag, searchTagList)) {
				a.setLimit(taglimit);
				listOfTags.add(a);
				count++;
				LOG.info("count = " + count);
			}
			**/
		}
		
		return listOfTags;
		
	}
	
	
	/**
	 * splitToMap
	 * 
	 * @param source
	 * @param entriesSeparator
	 * @param keyValueSeparator
	 * @return Map<String, String>
	 */
	public static Map<String, String> splitToMap(String source, String entriesSeparator, String keyValueSeparator) {
	    Map<String, String> map = new HashMap<String, String>();
	    source = source.substring(1,source.length()-1);
	    //LOG.info(source);
	    String[] entries = source.split(entriesSeparator);
	    //LOG.info("entries="+entries.toString());
	    for (int i=0;i<entries.length;i++) 
	    {
	        String entry = entries[i];
	        //LOG.info("current entry="+entry);
	        String[] keyValue = entry.split("=");
	        //LOG.info("keyValue="+keyValue.toString());
	        //LOG.info("key="+keyValue[0]);
	        //LOG.info("value="+keyValue[1]);
	        //handle null values in Asset if they appear
	        if (keyValue[1]!=null)
	        {
	        	map.put(keyValue[0], keyValue[1]);
	        }
	        else
	        {
	        	map.put(keyValue[0], "0");
	        }
	    }
	    return map;
	}
	
	/**
	 * generateTrainData
	 * 
	 * @param assetName
	 * @param request
	 * @return
	 
	public List<TrainingDataResponse> generateTrainData(TrainingDataParam trainDataParam){
		List<TrainingDataResponse> trainData = null;
		try{
		
			trainData = trainDataGenerator.generateTrainData(trainDataParam);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return trainData;
	}
	**/
	
	/**
	 * IngestAssetData
	 * 
	 * @param assetName
	 * @param request
	 * @return
	 */
	public CarbonAsset[] ingestCarbonData(Map<String,String>[] carbonMap) {

		try {			
			String url = predixAssetServiceUrl + "/carbon";
			Header tokenHeader = client.getTimeseriesHeaders().get(0);
			//String token = tokenHeader.getValue();
			String token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiI5NTYxMmE5MzkyMzY0MWI3YTQzMjUwOGU2NWIxMzMzZiIsInN1YiI6ImNsaWVudCIsInNjb3BlIjpbInRpbWVzZXJpZXMuem9uZXMuMWFkOGVmMTItM2NiNy00NGI3LWE0ZWItNjVjYzM4ZjFjNTgyLmluZ2VzdCIsInByZWRpeC1hc3NldC56b25lcy45NTIzY2E3Ni00ODgyLTQ5ZGEtYTNjMS0wMWRlN2M4MGZiZjMudXNlciIsInVhYS5yZXNvdXJjZSIsImludGVsbGlnZW50LW1hcHBpbmcuem9uZXMuNWJiYTgyYWItOTY4Yy00Yjc3LTlhM2UtODVjYmE2NzZhNDM2LnVzZXIiLCJvcGVuaWQiLCJ1YWEubm9uZSIsInRpbWVzZXJpZXMuem9uZXMuMWFkOGVmMTItM2NiNy00NGI3LWE0ZWItNjVjYzM4ZjFjNTgyLnF1ZXJ5IiwiYW5hbHl0aWNzLnpvbmVzLjY3NDFmMDI4LWE5NWMtNGNjYy1iOTk1LTVhNjcyMDg0NzU0Yy51c2VyIiwiYW5hbHl0aWNzLnpvbmVzLjExMGMxMTU3LWUzYTItNDhkMy1hZjNkLTkwNmYxMmIzMmZkZS51c2VyIiwidGltZXNlcmllcy56b25lcy4xYWQ4ZWYxMi0zY2I3LTQ0YjctYTRlYi02NWNjMzhmMWM1ODIudXNlciJdLCJjbGllbnRfaWQiOiJjbGllbnQiLCJjaWQiOiJjbGllbnQiLCJhenAiOiJjbGllbnQiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImUyNjU3MDMiLCJpYXQiOjE0OTczMzkyOTEsImV4cCI6MTQ5NzM4MjQ5MSwiaXNzIjoiaHR0cHM6Ly8xYWI2Nzc1ZC1kMmZhLTQxZDgtOGNkMi1kODFhNjk1NmI1YTQucHJlZGl4LXVhYS5ydW4uYXdzLXVzdzAyLXByLmljZS5wcmVkaXguaW8vb2F1dGgvdG9rZW4iLCJ6aWQiOiIxYWI2Nzc1ZC1kMmZhLTQxZDgtOGNkMi1kODFhNjk1NmI1YTQiLCJhdWQiOlsidGltZXNlcmllcy56b25lcy4xYWQ4ZWYxMi0zY2I3LTQ0YjctYTRlYi02NWNjMzhmMWM1ODIiLCJhbmFseXRpY3Muem9uZXMuNjc0MWYwMjgtYTk1Yy00Y2NjLWI5OTUtNWE2NzIwODQ3NTRjIiwidWFhIiwib3BlbmlkIiwicHJlZGl4LWFzc2V0LnpvbmVzLjk1MjNjYTc2LTQ4ODItNDlkYS1hM2MxLTAxZGU3YzgwZmJmMyIsImludGVsbGlnZW50LW1hcHBpbmcuem9uZXMuNWJiYTgyYWItOTY4Yy00Yjc3LTlhM2UtODVjYmE2NzZhNDM2IiwiY2xpZW50IiwiYW5hbHl0aWNzLnpvbmVzLjExMGMxMTU3LWUzYTItNDhkMy1hZjNkLTkwNmYxMmIzMmZkZSJdfQ.LFtuIUGWz4kzIoVeK6mvtVjph0yxV7lhzcO7wvWwEKZSxphrTWncyUV-ixf_WryqlXcjetfde0GZSTDBO5AwNdgOckjoY69S_uQorMhup1Bxnko5v8Or5ZCIjnEWHrP5b_9w9R9QiwcdPCfTWJK5dwdOzWnW2Ta4yvyn0S_nOyvbh3VRGv4PpEBa8iDl1ExKg263uOAgR6Qb5XkMOxEkYcokhWK8cXGNtuV8iSDXvVfaPpliQMcX0wZ6URLYWfsuAivAUNMNDvMnUBWbi_zlfBVw94G63YQGYphG812cuK0etTjd9pEUFjVMXEaC9oQIfLe3Uk1f7MRuOaHs1Fi6Rw";

			LOG.info("Ingest asset token" + token);
			LOG.info("assetzoneid=" +assetZoneId);
			CarbonAsset[] output = new CarbonAsset[168];
			String key = "";
			String uri = "";
						
			
			for (int i=0;i<168;i++) 
		    {
				CarbonAsset cAsset = new CarbonAsset();
				key = "["+(int)Math.floor(i/24)+","+i%24+"]";
				uri = "/carbon/"+(i+1);
				LOG.info("key="+key+" uri="+uri);
				cAsset.setUri(uri);
				String mean = carbonMap[0].get(key);
				LOG.info("mean="+mean);
				cAsset.setMean(Double.parseDouble(mean));
				cAsset.setStd(Double.parseDouble(carbonMap[1].get(key)));
				output[i] = cAsset;
				cAsset = null;
				key = null;
				uri = null;
				mean = null;
		    }
		    
			
			
			RestTemplate rt = new RestTemplate();
			rt.getMessageConverters().add(new StringHttpMessageConverter());

			// Map<String, String> validateApikeyRequestMap = new
			// HashMap<String, String>();

			HttpHeaders validateHeaders = new HttpHeaders();
			validateHeaders.setContentType(MediaType.APPLICATION_JSON);
			validateHeaders.add(MLConstants.HEADER_AUTHORIZATION, token);
			validateHeaders.add(MLConstants.HEADER_PREDIX_ZONEID, assetZoneId);
			
			//Post to Asset
			HttpEntity<Object> validityEntity = new HttpEntity<Object>(output, validateHeaders);
			LOG.info("headers" + validityEntity.toString());
			ResponseEntity<String> responseEntity = rt.exchange(url, HttpMethod.POST, validityEntity, String.class);
			LOG.info("response="+responseEntity.getBody());

			return output;
		} catch (Exception ex) {
			LOG.error("Error in ingestAssetData --", ex);
		}
		
		return null;

		
	}
}
